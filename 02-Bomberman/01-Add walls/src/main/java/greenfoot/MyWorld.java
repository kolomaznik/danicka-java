package greenfoot;

import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.World;

public class MyWorld extends World {
    /**
     * Create a new world with 8x8 cells and
     * with a cell size of 60x60 pixels
     */
    public MyWorld() {
        super(8, 8, 100);
        this.setBackground("images/world-background.png");

        addObject(new Fox(), 3, 5);
    }
}